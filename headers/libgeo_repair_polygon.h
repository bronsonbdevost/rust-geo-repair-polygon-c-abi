#include <cstdarg>
#include <cstdint>
#include <cstdlib>
#include <new>

struct BinaryData {
  const unsigned char *data;
    size_t len;
};

extern "C" {

void c_bin_data_free(BinaryData bin_data);

void c_char_free(char *ptr);

BinaryData join_multi_polygon(const unsigned char *in_data, size_t len);

char *join_multi_polygon_wkt(char *wkt);

BinaryData repair_wkb(const unsigned char *in_data, size_t len);

char *repair_wkt(char *wkt);

} // extern "C"
