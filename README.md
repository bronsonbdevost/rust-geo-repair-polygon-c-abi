# rust-geo-repair-polygon-c-abi

Standard C ABI lib bindings for the `geo-repair-polygon` library

## Usage

Compile with `cargo build release`. Then copy the dynamic library from `./target/release/libgeo_repair_polygon.{so/dylib/dll}` to wherever it is needed.  Headers can be found in `./headers/libgeo_repair_polygon.h`.

Each function passes a string/object with memory managed by this Rust library.  Make sure to call `c_bin_data_free` when using `join_multi_polygon` or `repair_wkb`, and use `c_char_free` with `join_multi_polygon_wkt` and `repair_wkt`.  Otherwise you have a memory leak.

The `BinaryData` struct is a simple handle for a byte array; `len` provides the length of the array, and `data` is a pointer to the memory address where it is located, serialize accordingly.