use geo_repair_polygon::join::Join;
use geo_repair_polygon::repair::Repair;
use geo_types::Geometry;
use geo_wkt_writer::ToWkt;
use std::ffi::CString;
use std::os::raw::{c_char, c_uchar};
use wkb::{geom_to_wkb, wkb_to_geom};
use wkt::Wkt;

#[repr(C)]
pub struct BinaryData {
    data: *const c_uchar,
    len: usize,
}

#[no_mangle]
pub extern "C" fn repair_wkb(in_data: *const c_uchar, len: usize) -> BinaryData {
    unsafe {
        // Throw a null object back immediately for bad input data (An empty point is 21 bytes)
        if in_data.is_null() || len < 21 {
            return BinaryData {
                data: *std::ptr::null(),
                len: 0,
            };
        }

        // Read the binary data into a Geometry
        let mut bin_data = std::slice::from_raw_parts(in_data, len);
        let geom = match wkb_to_geom(&mut bin_data) {
            Ok(g) => g,
            Err(_) => {
                return BinaryData {
                    data: *std::ptr::null(),
                    len: 0,
                }
            }
        };
        std::mem::forget(bin_data);

        // Find the right Geometry type and repair accordingly
        let geom_wkb = match geom {
            Geometry::MultiPolygon { .. } => {
                let parsed_geom = match geom.into_multi_polygon() {
                    Some(g) => g,
                    None => {
                        return BinaryData {
                            data: *std::ptr::null(),
                            len: 0,
                        }
                    }
                };
                let repaired_geom = match parsed_geom.repair() {
                    Some(g) => g,
                    None => {
                        return BinaryData {
                            data: *std::ptr::null(),
                            len: 0,
                        }
                    }
                };
                geom_to_wkb(&repaired_geom.join().into())
            }
            Geometry::Polygon { .. } => {
                let parsed_geom = match geom.into_polygon() {
                    Some(g) => g,
                    None => {
                        return BinaryData {
                            data: *std::ptr::null(),
                            len: 0,
                        }
                    }
                };
                let repaired_geom = match parsed_geom.repair() {
                    Some(g) => g,
                    None => {
                        return BinaryData {
                            data: *std::ptr::null(),
                            len: 0,
                        }
                    }
                };
                geom_to_wkb(&repaired_geom.into())
            }
            _ => geom_to_wkb(&geom),
        };

        // Package the binary data for the return
        let data = geom_wkb.as_ptr();
        let len = geom_wkb.len();
        std::mem::forget(geom_wkb);
        BinaryData { data, len }
    }
}

#[no_mangle]
pub extern "C" fn join_multi_polygon(in_data: *const c_uchar, len: usize) -> BinaryData {
    unsafe {
        // Throw a null object back immediately for bad input data (An empty point is 21 bytes)
        if in_data.is_null() || len < 21 {
            return BinaryData {
                data: *std::ptr::null(),
                len: 0,
            };
        }

        // Read the binary data into a Geometry
        let mut bin_data = std::slice::from_raw_parts(in_data, len);
        let geom = match wkb_to_geom(&mut bin_data) {
            Ok(g) => g,
            Err(_) => {
                return BinaryData {
                    data: *std::ptr::null(),
                    len: 0,
                }
            }
        };
        std::mem::forget(bin_data);

        // Find the right Geometry type and repair accordingly
        let geom_wkb = match geom {
            Geometry::MultiPolygon { .. } => {
                let parsed_geom = match geom.into_multi_polygon() {
                    Some(g) => g,
                    None => {
                        return BinaryData {
                            data: *std::ptr::null(),
                            len: 0,
                        }
                    }
                };
                geom_to_wkb(&parsed_geom.join().into())
            }
            _ => geom_to_wkb(&geom),
        };

        // Package the binary data for the return
        let data = geom_wkb.as_ptr();
        let len = geom_wkb.len();
        std::mem::forget(geom_wkb);
        BinaryData { data, len }
    }
}

#[no_mangle]
pub extern "C" fn c_bin_data_free(bin_data: BinaryData) {
    // Only free something that exists
    if !bin_data.data.is_null() {
        // Read the data into a Box
        let decoded = unsafe {
            std::slice::from_raw_parts_mut(bin_data.data as *mut u8, bin_data.len as usize)
        };
        let decoded = decoded.as_mut_ptr();
        let boxed = unsafe { Box::from_raw(decoded) };
        drop(boxed) // Explicitly drop the data
    }
}

#[no_mangle]
pub extern "C" fn repair_wkt(wkt: *mut c_char) -> *mut c_char {
    unsafe {
        // Parse the string
        let rust_wkt = match CString::from_raw(wkt).into_string() {
            Ok(wkt_geom) => wkt_geom,
            Err(_) => return CString::new("INVALIDGEOMETRY").unwrap().into_raw(),
        };

        // Load the string as WKT into a Geometry
        let wkt_geom: Wkt<f64> = match Wkt::from_str(&rust_wkt) {
            Ok(geom) => geom,
            Err(_) => return CString::new("INVALIDGEOMETRY").unwrap().into_raw(),
        };

        let geom = match wkt::conversion::try_into_geometry(&wkt_geom.items[0]) {
            Ok(parsed_geom) => parsed_geom,
            Err(_) => return CString::new("INVALIDGEOMETRY").unwrap().into_raw(),
        };

        // Perform the necessary repair operation
        let repaired_geom = match geom {
            Geometry::MultiPolygon { .. } => {
                let parsed_geom = match geom.into_multi_polygon() {
                    Some(g) => g,
                    None => return CString::new("INVALIDGEOMETRY").unwrap().into_raw(),
                };
                let repaired_geom = match parsed_geom.repair() {
                    Some(g) => g,
                    None => return CString::new("INVALIDGEOMETRY").unwrap().into_raw(),
                };
                repaired_geom.join().to_wkt()
            }
            Geometry::Polygon { .. } => {
                let parsed_geom = match geom.into_polygon() {
                    Some(g) => g,
                    None => return CString::new("INVALIDGEOMETRY").unwrap().into_raw(),
                };
                let repaired_geom = match parsed_geom.repair() {
                    Some(g) => g,
                    None => return CString::new("INVALIDGEOMETRY").unwrap().into_raw(),
                };
                repaired_geom.to_wkt()
            }
            Geometry::MultiLineString { .. } => {
                let parsed_geom = match geom.into_multi_line_string() {
                    Some(g) => g,
                    None => return CString::new("INVALIDGEOMETRY").unwrap().into_raw(),
                };
                parsed_geom.to_wkt()
            }
            Geometry::LineString { .. } => {
                let parsed_geom = match geom.into_line_string() {
                    Some(g) => g,
                    None => return CString::new("INVALIDGEOMETRY").unwrap().into_raw(),
                };
                parsed_geom.to_wkt()
            }
            Geometry::Line { .. } => {
                let parsed_geom = match geom.into_line() {
                    Some(g) => g,
                    None => return CString::new("INVALIDGEOMETRY").unwrap().into_raw(),
                };
                parsed_geom.to_wkt()
            }
            _ => geom.to_wkt(),
        };

        // Return the WKT as a C string
        CString::new(repaired_geom).unwrap().into_raw()
    }
}

#[no_mangle]
pub extern "C" fn join_multi_polygon_wkt(wkt: *mut c_char) -> *mut c_char {
    unsafe {
        // Parse the string
        let rust_wkt = match CString::from_raw(wkt).into_string() {
            Ok(wkt_geom) => wkt_geom,
            Err(_) => return CString::new("INVALIDGEOMETRY").unwrap().into_raw(),
        };

        // Load the string as WKT into a Geometry
        let wkt_geom: Wkt<f64> = match Wkt::from_str(&rust_wkt) {
            Ok(geom) => geom,
            Err(_) => return CString::new("INVALIDGEOMETRY").unwrap().into_raw(),
        };

        let geom = match wkt::conversion::try_into_geometry(&wkt_geom.items[0]) {
            Ok(parsed_geom) => parsed_geom,
            Err(_) => return CString::new("INVALIDGEOMETRY").unwrap().into_raw(),
        };

        // Perform the necessary repair operation
        let repaired_geom = match geom {
            Geometry::MultiPolygon { .. } => {
                let parsed_geom = match geom.into_multi_polygon() {
                    Some(g) => g,
                    None => return CString::new("INVALIDGEOMETRY").unwrap().into_raw(),
                };
                parsed_geom.join().to_wkt()
            }
            _ => geom.to_wkt(),
        };

        // Return the WKT as a C string
        CString::new(repaired_geom).unwrap().into_raw()
    }
}

#[no_mangle]
pub extern "C" fn c_char_free(ptr: *mut c_char) {
    if !ptr.is_null() {
        let boxed = unsafe { CString::from_raw(ptr) };
        drop(boxed) // Explicitly drop the data
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_repair_wkt() {
        let bad = "POLYGON((0 0,10 0,10 10,0 10,0 0),(1 1,1 8,3 8,3 1,1 1),(3 1,3 8,5 8,5 1,3 1))";

        let response = repair_wkt(CString::new(bad).unwrap().into_raw());
        let expected = "POLYGON((0 0,10 0,10 10,0 10,0 0),(1 1,1 8,2.9999999999999982 7.9999999999999964,2.9999999999999982 1,1 1),(3 1,3.0000000000000018 1.0000000000000009,3.0000000000000018 7.9999999999999964,5 8,5 1,3 1))";

        unsafe {
            let answer = match CString::from_raw(response).into_string() {
                Ok(wkt_geom) => wkt_geom,
                Err(_) => "none".into(),
            };
            assert_eq!(answer, expected);
        }
    }

    #[test]
    fn can_repair_wkb() {
        unsafe {
            let bad =
                "POLYGON((0 0,10 0,10 10,0 10,0 0),(1 1,1 8,3 8,3 1,1 1),(3 1,3 8,5 8,5 1,3 1))";
            let wkt_geom: Wkt<f64> = Wkt::from_str(&bad).unwrap();
            let geom = wkt::conversion::try_into_geometry(&wkt_geom.items[0]).unwrap();
            let wkb = geom_to_wkb(&geom);
            let data = wkb.as_ptr();
            let len = wkb.len();
            let bin_data = BinaryData { data, len };

            let response = repair_wkb(bin_data.data, bin_data.len);
            let mut bin_data = std::slice::from_raw_parts(response.data, response.len as usize);
            let geom = wkb_to_geom(&mut bin_data).unwrap();
            c_bin_data_free(response);
            let wkt = geom.to_wkt();

            let expected = "POLYGON((0 0,10 0,10 10,0 10,0 0),(1 1,1 8,2.9999999999999982 7.9999999999999964,2.9999999999999982 1,1 1),(3 1,3.0000000000000018 1.0000000000000009,3.0000000000000018 7.9999999999999964,5 8,5 1,3 1))";
            assert_eq!(wkt, expected);
        }
    }

    #[test]
    fn can_join_wkt_multi_polygon() {
        let bad =
            "MULTIPOLYGON (((0 0, 0 10, 10 10, 10 0, 0 0)),((11 11, 11 20, 20 20, 20 11, 11 11)))";

        let response = join_multi_polygon_wkt(CString::new(bad).unwrap().into_raw());
        let expected = "POLYGON((10 10,11 11,20 11,20 20,11 20,11 11.000000000000004,9.999999999999996 10,0 10,0 0,10 0,10 10))";

        unsafe {
            let answer = match CString::from_raw(response).into_string() {
                Ok(wkt_geom) => wkt_geom,
                Err(_) => "none".into(),
            };
            assert_eq!(answer, expected);
        }
    }

    #[test]
    fn can_join_wkb_multi_polygon() {
        unsafe {
            let bad =
                "MULTIPOLYGON (((0 0, 0 10, 10 10, 10 0, 0 0)),((11 11, 11 20, 20 20, 20 11, 11 11)))";
            let wkt_geom: Wkt<f64> = Wkt::from_str(&bad).unwrap();
            let geom = wkt::conversion::try_into_geometry(&wkt_geom.items[0]).unwrap();
            let wkb = geom_to_wkb(&geom);
            let data = wkb.as_ptr();
            let len = wkb.len();
            let bin_data = BinaryData { data, len };

            let response = join_multi_polygon(bin_data.data, bin_data.len);
            let mut bin_data = std::slice::from_raw_parts(response.data, response.len as usize);
            let geom = wkb_to_geom(&mut bin_data).unwrap();
            c_bin_data_free(response);
            let wkt = geom.to_wkt();

            let expected = "POLYGON((10 10,11 11,20 11,20 20,11 20,11 11.000000000000004,9.999999999999996 10,0 10,0 0,10 0,10 10))";
            assert_eq!(wkt, expected);
        }
    }
}
